#!/bin/bash
export PUBLISHER_APP_INTERVAL="3s"

export PUBLISHER_NATS_HOST = "nats_service_nats_server"
export PUBLISHER_NATS_PORT = "4222"