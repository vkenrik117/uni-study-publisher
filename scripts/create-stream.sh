#!/bin/bash
nats stream add STUDY --subjects "STUDY.*" --ack --max-msgs=-1 --max-bytes=-1 --max-age=1y --storage file --retention limits --max-msg-size=-1 --discard=old
