package nats

import (
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	"go.uber.org/zap"
	"time"
)

const defaultDataQuantity = 10

type DataGetter interface {
	ProduceData() []byte
}

type NATS struct {
	dataGetter DataGetter
	subjects   []string
	interval   time.Duration
	connect    *nats.Conn
	stream     nats.JetStream
	logger     *zap.Logger
}

func New(dataGetter DataGetter, interval time.Duration, connect *nats.Conn, logger *zap.Logger) (*NATS, error) {
	stream, err := connect.JetStream(nats.PublishAsyncMaxPending(256))
	if err != nil {
		return nil, fmt.Errorf("get JetStream: %s", err.Error())
	}

	return &NATS{
		dataGetter: dataGetter,
		subjects:   make([]string, 0),
		interval:   interval,
		connect:    connect,
		stream:     stream,
		logger:     logger,
	}, nil
}

// AddSubject добавляет новый subject, она же тема сообщений, в который NATS будет публиковать сообщения
func (n *NATS) AddSubject(subject string) {
	n.subjects = append(n.subjects, subject)
}

func (n *NATS) Start(ctx context.Context) error {
	ticker := time.NewTicker(n.interval)

OuterLoop:
	for {
		select {
		case <-ticker.C:
			if n.connect.IsClosed() {
				ticker.Stop()
				break OuterLoop
			}

			data := n.getSomeData(defaultDataQuantity)
			for _, dataSet := range data {
				pubAck, err := n.stream.PublishAsync("defaultSubj", dataSet)
				if err != nil {
					return fmt.Errorf("could not publish data asynchronously: %s", err.Error())
				}
				select {
				case <-pubAck.Ok():
					n.logger.Info("data has been successfully acknowledged")
				case err := <-pubAck.Err():
					n.logger.Error("data publishing was unsuccessful", zap.Error(err))
				}
			}

		}
	}

	return nil
}

func (n *NATS) Stop() {
	if !n.connect.IsClosed() {
		n.connect.Close()
	}
	n.logger.Info("stream loop stopped")
}

func (n *NATS) getSomeData(dataQuantity int) [][]byte {
	data := make([][]byte, dataQuantity)
	for i := 0; i < dataQuantity; i++ {
		data = append(data, n.dataGetter.ProduceData())
	}
	return data
}
