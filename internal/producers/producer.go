package producers

import (
	"go.uber.org/zap"
	"math/rand"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6
	letterIdxMask = 1<<letterIdxBits - 1
)
const defaultDataLength = 10

// RandomDataProducer генератор случайных данных, а именно случайных строк с фиксированной длинной
type RandomDataProducer struct {
	dataLength int
	logger     *zap.Logger
}

func NewRandomProducer(dataLength int, logger *zap.Logger) *RandomDataProducer {
	if dataLength <= 0 {
		dataLength = defaultDataLength
	}
	return &RandomDataProducer{
		dataLength: dataLength,
		logger:     logger,
	}
}

func (p RandomDataProducer) ProduceData() []byte {
	b := make([]byte, p.dataLength)
	for i := 0; i < p.dataLength; {
		if idx := int(rand.Int63() & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i++
		}
	}
	return b
}
