package configs

import (
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"time"
)

// AppConfig конфигурация приложения
type AppConfig struct {
	// Interval это период времени между публикациями в NATS
	Interval time.Duration `envconfig:"PUBLISHER_APP_INTERVAL"`
}

func NewAppConfig() (*AppConfig, error) {
	var appConfig AppConfig
	err := envconfig.Process("publisher_app", &appConfig)
	if err != nil {
		return nil, fmt.Errorf("could not process app env: %s", err.Error())
	}
	return &appConfig, nil
}
