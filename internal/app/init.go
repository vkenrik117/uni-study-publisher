package app

import (
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	"gitlab.com/vkenrik117/uni-study-publisher/internal/configs"
	worker "gitlab.com/vkenrik117/uni-study-publisher/internal/nats"
	"gitlab.com/vkenrik117/uni-study-publisher/internal/producers"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

// App хранит в себе поля с сущностями для работы с брокерами сообщений,
// добавьте новый брокер сообщений, если вы реализовали методы для публикации сообщений
type App struct {
	nats *worker.NATS
}

func Initialize(ctx context.Context) (*App, error) {
	logger, err := zap.NewDevelopment()
	if err != nil {
		return nil, err
	}

	appConfig, err := configs.NewAppConfig()
	if err != nil {
		return nil, err
	}
	natsConfig, err := configs.NewConfigNATS()
	if err != nil {
		return nil, err
	}

	natsConnectionString := fmt.Sprintf("nats://%s:%s",
		natsConfig.Host,
		natsConfig.Port)
	connection, err := nats.Connect(natsConnectionString)
	if err != nil {
		return nil, fmt.Errorf("connect: %s", err.Error())
	}

	randomProducer := producers.NewRandomProducer(0, logger)

	newWorker, err := worker.New(randomProducer, appConfig.Interval, connection, logger)

	newWorker.AddSubject("STUDY.*")

	return &App{nats: newWorker}, nil
}

func (a *App) Run(ctx context.Context) error {
	errGroup, ctx := errgroup.WithContext(ctx)

	errGroup.Go(func() error {
		return a.nats.Start(ctx)
	})
	errGroup.Go(func() error {
		<-ctx.Done()
		a.nats.Stop()
		return nil
	})

	return errGroup.Wait()
}
